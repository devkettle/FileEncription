#include <stdio.h>
#include "Keys.h"
#include "Encryption.h"
#include <string.h>
#include <stdlib.h>


int stringEncDec(struct PublicKeys *publicKeys, struct PrivateKeys *privateKeys);

int fileEncDec(struct PublicKeys *publicKeys, struct PrivateKeys *privateKeys);

int workCycle();

int main() {
    /*
    struct Keys myKeys;
    generateKeys(&myKeys);
    printf("q=%u\n", myKeys.q);
    printf("p=%u\n", myKeys.p);
    printf("n=%u\n", myKeys.n);
    printf("fi=%u\n", myKeys.fi);
    printf("e=%u\n", myKeys.e);
    printf("d=%u\n", myKeys.d);

    struct PublicKeys publicKeys;
    struct PrivateKeys privateKeys;
    publicKeys = getPublicKeys(&myKeys);
    privateKeys = getPrivateKeys(&myKeys);
     */
    return workCycle();
}

int stringEncDec(struct PublicKeys *publicKeys, struct PrivateKeys *privateKeys) {
    char c;
    char *str1;
    str1 = (char *) calloc(400, 1);
    fgets(str1, 64, stdin);
    int i = (int) (strlen(str1) - 1);
    if (str1[i] == '\n') str1[i] = '\0';
    while (str1[0] != 'x') {
        if (str1[0] == '\n') {
            fgets(str1, 64, stdin);
            i = (int) (strlen(str1) - 1);
            if (str1[i] == '\n') str1[i] = '\0';
            continue;
        }
        printf("string=%s\n", str1);
        str1 = encryptString(str1, publicKeys);
        printf("encrypted=%s\n", str1);
        str1 = decryptString(str1, privateKeys);
        printf("decrypted=%s\n", str1);
        fgets(str1, 64, stdin);
        i = (int) (strlen(str1) - 1);
        if (str1[i] == '\n') str1[i] = '\0';
    }
    free(str1);
    return 0;

}

extern int encryptFile(char *fileName, struct PublicKeys *pc);

extern int decryptFile(char *fileName, struct PrivateKeys *pc);
int fileEncDec(struct PublicKeys *publicKeys, struct PrivateKeys *privateKeys) {
    char *fileName;
    fileName = (char *) calloc(400, 1);
    fgets(fileName, 64, stdin);
    int i = (int) (strlen(fileName) - 1);
    if (fileName[i] == '\n') fileName[i] = '\0';
    char *encFileName = fileName/*"text.txt"*/;
    char *decFileName = (char *) calloc(400, 1)/*"text.txt.enc"*/;
    strcpy(decFileName, fileName);
    strcat(decFileName, ".enc");
    encryptFile(encFileName, publicKeys);
    decryptFile(decFileName, privateKeys);
    return 0;
}

int workCycle() {
    printf("Welcome to the Encription program!\n"
                   "Type needed symbol to make program do what you want:\n"
                   " e to encypt file\n"
                   " d to decrypt file\n"
                   " g to generate keys\n"
                   " k to edit keys\n"
                   " x to exit\n");
    char cToDo, c;
    int i;
    scanf("%c", &cToDo);
    struct Keys keys;
    struct PublicKeys publicKeys;
    struct PrivateKeys privateKeys;
    char *fileName;
    fileName = (char *) calloc(400, 1);
    keys.p = 0;
    keys.e = 0;
    keys.d = 0;
    keys.fi = 0;
    keys.n = 0;
    keys.q = 0;
    while (cToDo != 'x') {
        switch (cToDo) {
            case 'g':
                generateKeys(&keys);
                publicKeys = getPublicKeys(&keys);
                privateKeys = getPrivateKeys(&keys);
                printf("e=%u\n", keys.e);
                printf("d=%u\n", keys.d);
                printf("n=%u\n", keys.n);
                scanf("%c", &c);
                break;
            case 'e':
                if (publicKeys.e == 0 || publicKeys.n == 0) {
                    generateKeys(&keys);
                    publicKeys = getPublicKeys(&keys);
                    privateKeys = getPrivateKeys(&keys);
                }
                printf("e=%u\n", keys.e);
                printf("d=%u\n", keys.d);
                printf("n=%u\n", keys.n);
                printf("Type name of the file you want to encrypt\n");
                scanf("%c", &c);
                fgets(fileName, 64, stdin);
                i = (int) (strlen(fileName) - 1);
                if (fileName[i] == '\n') fileName[i] = '\0';
                encryptFile(fileName, &publicKeys);
                break;
            case 'd':
                if (privateKeys.d == 0 || publicKeys.n == 0) {
                    printf("There no key to decrypt with. Type e or g to edit keys\n");
                    scanf("%c", &c);
                    scanf("%c", &cToDo);
                    continue;
                }
                printf("e=%u\n", keys.e);
                printf("d=%u\n", keys.d);
                printf("n=%u\n", keys.n);
                printf("Type name (without .enc postfix) of the file you want to decrypt\n");
                scanf("%c", &c);
                fgets(fileName, 64, stdin);
                i = (int) (strlen(fileName) - 1);
                if (fileName[i] == '\n') fileName[i] = '\0';
                char *decFileName = (char *) calloc(400, 1)/*"text.txt.enc"*/;
                strcpy(decFileName, fileName);
                strcat(decFileName, ".enc");
                decryptFile(decFileName, &privateKeys);
                free(decFileName);
                break;
            case 'k':
                if (keys.e != 0) {
                    printf("Previous keys were:\n");
                    printf("e=%u\n", keys.e);
                    printf("d=%u\n", keys.d);
                    printf("n=%u\n", keys.n);
                } else {
                    printf("Previous keys were empty\n");
                }
                keys.e = keys.d = keys.n = 0;
                while (keys.e < 1) {
                    printf("Enter e, it must be more than 1:\n e=");
                    scanf("%u", &keys.e);
                    printf("\n");
                }
                while (keys.d < 1) {
                    printf("Enter d, it must be more than 1:\n d=");
                    scanf("%u", &keys.d);
                    printf("\n");
                }
                while (keys.n <= 256) {
                    printf("Enter n, it must be more than 256:\n n=");
                    scanf("%u", &keys.n);
                    printf("\n");
                }
                publicKeys = getPublicKeys(&keys);
                privateKeys = getPrivateKeys(&keys);
                printf("Now keys are:\n");
                printf("e=%u\n", keys.e);
                printf("d=%u\n", keys.d);
                printf("n=%u\n", keys.n);
                scanf("%c", &c);
                break;
            default:
                scanf("%c", &c);
                printf("Try again\n");
                break;
        }
        scanf("%c", &cToDo);
    }
    free(fileName);
    return 0;
}
