#include "Keys.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int generateKeys(struct Keys *keys);

unsigned int getEByFi(unsigned int fi);

unsigned int getDByFiAndE(unsigned int fi, unsigned int e);

int generatePrimeInt(int notEqual, int moreThan);

char *chrcat(char *str, char chr);

struct PublicKeys getPublicKeys(struct Keys *keys);

struct PrivateKeys getPrivateKeys(struct Keys *keys);

int generateKeys(struct Keys *keys) {
    long int startTime = (long int) time(NULL);
    keys->d = 0;
    while (keys->d < 2 || keys->d == 4294967295) {
        printf("Generating keys\n");
        int minInt = 32;
        keys->p = (unsigned int) generatePrimeInt(0, minInt);
        keys->q = (unsigned int) generatePrimeInt(keys->p, minInt);
        keys->n = (keys->p) * (keys->q);
        keys->fi = (keys->p - 1) * (keys->q - 1);
        keys->e = getEByFi(keys->fi);
        //printf("Keys without d generated in %ld seconds\n", ((long int) time(NULL) - startTime));
        keys->d = getDByFiAndE(keys->fi, keys->e);
    }
    printf("Keys generated in %ld seconds\n", ((long int) time(NULL) - startTime));
    return 0;
}

unsigned int getEByFi(unsigned int fi) {
    unsigned int lastGoodI = 0;
    FILE *fp = NULL;
    char c;

    fp = fopen("primeNumbers.txt", "r");

    char *str = (char *) calloc(16, sizeof(char));

    unsigned int i = 2, lasti = 2;
    while (!feof(fp)) {
        c = (char) fgetc(fp);
        srand(time(NULL));
        if (c >= '0' && c <= '9') {
            chrcat(str, c);
        } else {
            lasti = i;
            i = (unsigned int) atoi(str);
            free(str);
            if (i) {
                if (fi % i && i < fi) {
                    lastGoodI = i;
                    if (!(rand() % 16)) {
                        fclose(fp);
                        return i;
                    }
                }
            } else {
                i = lasti + 1;
            }
            str = (char *) calloc(16, sizeof(char));
        }
    }
    free(str);
    fclose(fp);

    fp = fopen("primeNumbers.txt", "a");
    //fputc('3',fp);
    for (; i <= fi; ++i) {
        unsigned int j = 2;
        for (; j < i / 2 + 1; ++j) {
            if (i % j == 0)
                break;
        }
        if (i % j == 0)
            continue;
        else {
            char cs[10];
            int itos = i;
            int k = 0;
            while (itos) {
                int x = itos % 10;
                cs[k] = x + '0';
                itos = itos / 10;
                k++;
            }
            k--;
            for (; k >= 0; k--) {
                putc(cs[k], fp);
            }
            putc('\r', fp);
            putc('\n', fp);
        }
        if (fi % i == 0)
            continue;
        else {
            lastGoodI = i;
            if ((rand() % 16))
                continue;
        }
        fclose(fp);
        return i;
    }
    return lastGoodI;
}

int generatePrimeInt(int notEqual, int moreThan) {
    int max = 1024;
    int chance = 8;
    FILE *fp = NULL;
    fp = fopen("primeNumbers.txt", "r");
    char c;
    char *str = (char *) calloc(16, sizeof(char));
    int a = 2;
    srand(time(NULL));
    while (!feof(fp)) {
        c = (char) fgetc(fp);
        srand(time(NULL) + rand() % 16);
        if (c >= '0' && c <= '9') {
            chrcat(str, c);
        } else {
            a = (unsigned int) atoi(str);
            free(str);
            if (a) {
                if (a >= moreThan && a != notEqual && !(rand() % chance) && a < max) {
                    fclose(fp);
                    return a;
                }
            }
            if (a >= max) {
                fclose(fp);
                fp = fopen("primeNumbers.txt", "r");
            }
            str = (char *) calloc(16, sizeof(char));
            chance = (int) (chance * 1.01);
        }
    }
    free(str);
    fclose(fp);
    return a;
}

unsigned int getDByFiAndE(unsigned int fi, unsigned int e) {
    long d = 1;
    long e_ = e;
    long fi_ = fi;
    long maxInt = (unsigned int) (-1);
    for (; ((d * e_) % fi_ != 1); d++) {
        if (d >= maxInt) {
            break;
        }
    }
    return (unsigned int) d;
}

struct PublicKeys getPublicKeys(struct Keys *keys) {
    struct PublicKeys pc;
    pc.e = keys->e;
    pc.n = keys->n;
    return pc;
}

struct PrivateKeys getPrivateKeys(struct Keys *keys) {
    struct PrivateKeys pc;
    pc.d = keys->d;
    pc.n = keys->n;
    return pc;
}

char *chrcat(char *str, char chr) {
    register char *tmp = str;
    int len = (int) (strlen(str) + 1);

    tmp = (char *) realloc(tmp, len + 1);

    tmp[len - 1] = chr;
    tmp[len] = 0;

    return tmp;
}


